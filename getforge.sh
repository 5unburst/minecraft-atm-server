#!/bin/bash

mkdir server
cd server

# link to jar : https://maven.minecraftforge.net/net/minecraftforge/forge/1.20.1-47.1.43/forge-1.20.1-47.1.43-installer.jar
MC_VERSION=$1
FORGE_BUILD=$2

echo Download forge version ${MC_VERSION}-${FORGE_BUILD}

DW_URL="https://maven.minecraftforge.net/net/minecraftforge/forge/${MC_VERSION}-${FORGE_BUILD}/forge-${MC_VERSION}-${FORGE_BUILD}-installer.jar"

curl -s -o "forge.jar" ${DW_URL}

echo Build ${MC_VERSION}-${FORGE_BUILD}

java -jar "forge.jar" --installServer

echo Remove artefact ${MC_VERSION}-${FORGE_BUILD}

rm "forge.jar"