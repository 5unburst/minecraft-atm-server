FROM eclipse-temurin:17-alpine AS build

RUN apk add curl bash

WORKDIR /opt/minecraft
COPY ./getforge.sh ./
RUN chmod +x getforge.sh
RUN ./getforge.sh "1.20.1" "47.1.3"

FROM eclipse-temurin:17-alpine AS runtime

COPY --from=build /opt/minecraft/server /server
WORKDIR /server

EXPOSE 25565/tcp
EXPOSE 25565/udp

COPY ./mods/ /server/mods
COPY ./config/ /server/config
COPY ./defaultconfigs/ /server/defaultconfigs
COPY ./eula.txt /server/eula.txt

CMD java @user_jvm_args.txt @libraries/net/minecraftforge/forge/1.20.1-47.1.3/unix_args.txt "$@"